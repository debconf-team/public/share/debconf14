\documentclass{beamer}
\usepackage{graphicx}
\usepackage{tikz}

\usepgflibrary{shapes}

\DeclareUrlCommand\email{}

\beamertemplatenavigationsymbolsempty

\newcommand{\bgcolor}[2]{
\setbeamercolor{background canvas}{bg=#1}
#2
\setbeamercolor{background canvas}{bg=white}
}

\title{A glimpse into a systemd future}
\author[Josh Triplett]{Josh Triplett\\\email{josh@joshtriplett.org}}
\date{DebConf 2014}

\hypersetup{pdftitle={A glimpse into a systemd future},pdfauthor={Josh Triplett}}

% Let's look at a future Debian system, taking full advantage of
% systemd components and features.

% This presentation will take an entirely different approach from past
% discussions of systemd in Debian. Rather than thinking about how to
% avoid or replace individual components, we'll look at how they fit
% together, and what unique functionality they provide.

% Finally, after exploring this world of the future, we'll return to
% the present and discuss ways to enable smooth transitions. We'll
% also explore facilities in systemd that support easier and better
% integrated selection of components, both for system services and
% within user sessions.

% Technologies covered include journald, systemd-networkd, socket
% activation, timer units, containers, and systemd user
% sessions. Goals include reducing boot time, reducing duplicate
% configuration, improving system manageability, improving battery
% life, and unifying graphical session startup.

\begin{document}
\maketitle

\begin{frame}{We've had a few discussions about systemd this year}
\includegraphics[height=0.9\textheight]{flammenwerfer}
\end{frame}

\begin{frame}{Discussion topics and results}
  \begin{itemize}
  \item Default: what does \texttt{/sbin/init} point to?\onslide<4->{ (systemd)}
\pause
  \item Can packages use systemd-specific functionality?\onslide<5->{ (yes)}
\pause
  \item Should packages support other inits?\onslide<6->{ (patches welcome)}
  \end{itemize}
\end{frame}

\begin{frame}
  \begin{center}
    \begin{tikzpicture}
      \node[shape=forbidden sign,draw,very thick] {\Huge shell};
    \end{tikzpicture}
    
    \LARGE systemd: replacement for \texttt{/etc/init.d}
  \end{center}
\end{frame}

\begin{frame}
  \begin{center}
    \LARGE systemd-logind and systemd-shim
  \end{center}
\end{frame}

\begin{frame}
\includegraphics[width=\textwidth]{skub}
\end{frame}

\begin{frame}{What haven't we talked about?}
  \begin{itemize}
  \item Debian Policy for systemd (in progress)
  \item Other systemd components and features
  \item systemd integration
  \end{itemize}
\end{frame}

\begin{frame}{Goals of this talk}
  \begin{itemize}
  \item Tour undiscussed and future systemd components
  \item Reduce future component-by-component flamewars
  \item Plan future transitions
  \end{itemize}
\end{frame}

\begin{frame}
  \begin{center}
    \begin{LARGE}
      \includegraphics[height=\baselineskip]{tourbus}

      Tour of systemd features
    \end{LARGE}
  \end{center}
\end{frame}

\begin{frame}{Common infrastructure across environments}
  \begin{itemize}
  \item Avoid desktop-specific implementations of system functionality
    \begin{itemize}
    \item Make functionality accessible to all environments
    \item Make the implementation common
    \end{itemize}
  \item Session management (logind)
  \item Power management, including suspend/resume
  \item Save/restore backlight and similar levels
  \item Save/restore rfkill for bluetooth and wireless
  \end{itemize}
\end{frame}

\begin{frame}{journald}
  \begin{itemize}
  \item Replacement for syslog
  \item Includes structured metadata
  \item Records unforgeable ``trusted'' metadata about processes
  \item Handles compression/rotation automatically
  \item Logs daemon output (stdout/stderr)
\pause
  \item Transition:
    \begin{itemize}
    \item Provide a package that enables persistent journaling
    \item Have that package provide system-log-daemon
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{logind-based power management}
  \begin{itemize}
  \item Replaces desktop-specific suspend logic and configuration
  \item Automatic: Software can block/delay until screen locked,
    connection disconnected, without desktop-specific code
  \item Manual: \texttt{systemd-inhibit wget ...}
  \item Configure suspend/resume in one place
\pause
  \item Transition:
    \begin{itemize}
    \item Desktop environments: don't manually suspend, just inhibit
    \item Applications: inhibit via systemd, not desktop APIs
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{systemd-based initramfs}
  \begin{itemize}
  \item Event-driven
  \item "What do I need to do to mount the root filesystem?"
  \item As with system startup, handles arbitrarily stacked services
    \begin{itemize}
    \item Arbitrary nesting of LVM, RAID, crypto, loop, smartcards,
      networking, NFS
    \end{itemize}
  \item pivots to / and preserves state
  \item shutdown returns to initramfs for clean umount of /
\pause
  \item Transition options:
    \begin{itemize}
    \item Port initramfs-tools to use systemd, and convert all hooks
      to systemd services
    \item Switch to dracut or another initramfs builder
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{nss-myhostname}
  \begin{itemize}
  \item Resolves the system hostname and ``localhost''
  \item Reduces hostname changes to a single file (/etc/hostname) or DBus call (systemd-hostnamed)
  \item /etc/hosts becomes optional
  \item Works in containers
  \item No good reason not to install and use it today
  \item We should default to this
  \end{itemize}
\end{frame}

\begin{frame}{systemd-networkd}
  \begin{itemize}
  \item Built-in DHCP client and server
  \item Does not spawn off other programs (such as dhclient)
  \item Robust
  \item High-performance (obtains a DHCP address in milliseconds)
  \item Designed for all the needs of servers, not just desktops
    \begin{itemize}
    \item Bridging, bonding, containers, tunnels, VLANs
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{systemd-machined and nss-mymachines}
  \begin{itemize}
  \item Tracks virtual machines and containers
  \item Robustly starts, stops, reboots, and respawns
  \item nss-mymachines resolves hostnames of VMs and containers
  \end{itemize}
\end{frame}

\begin{frame}{systemd-resolved}
  \begin{itemize}
  \item Local caching DNS resolver
  \item Handles multiple network interfaces, VPNs, captive portals
  \item Handles link-local name resolution for automatic connectivity
  \end{itemize}
\end{frame}

\begin{frame}{Socket activation}
  \begin{itemize}
  \item Parallelism: Launch in parallel with your dependencies
  \item No need to declare a dependency; just use what you need
  \item Avoid using resources for infrequently used services (CUPS)
  \end{itemize}
\end{frame}

\begin{frame}{kdbus}
  \begin{itemize}
  \item Kernel replacement for DBus daemon
  \item Eliminates extra context-switches
  \item Different transport, compatible higher-level library implementations
  \item Ports for various DBus libraries
  \item Available in extremely early boot or initramfs
\pause
  \item Kernel module available
  \item Too late to migrate for jessie
  \item Should migrate completely for jessie+1 and drop dbus-daemon
  \end{itemize}
\end{frame}

\begin{frame}{Containerization and security}
  \begin{itemize}
  \item Security and privilege-reduction via .service configuration
  \item Can easily launch a service in a container
  \item Create dynamic containers via systemd-nspawn
  \end{itemize}
\end{frame}

\begin{frame}{systemd.timer units}
  \begin{itemize}
  \item Minimizes wakeups using AccuracySec (default 1 min)
  \item Automatic anacron-like functionality, without polling
  \item Can wake the system from suspend
  \item Full unit file launch configuration available
  \end{itemize}
\end{frame}

\begin{frame}{Transient service units}
  \begin{itemize}
  \item Dynamically construct and launch a service without a .service file
  \item Run from a clean environment
  \item Detached/daemonized as normal
  \item Tracked as a normal service
  \end{itemize}
\end{frame}

\begin{frame}{sysusers}
  \begin{itemize}
  \item eliminating maintainer scripts in favor of declarative data
  \item Defaults to automatic allocation of next UID/GID
  \item Can hardcode UID/GID if necessary
  \item Can read UID/GID from file on disk
  \end{itemize}
\end{frame}

\begin{frame}{firstboot}
  \begin{itemize}
  \item Services run to set up an unconfigured system
  \item Create required system files (/etc, /var)
  \item Run sysusers to create users
  \item Allows running from empty /etc and /var
  \end{itemize}
\end{frame}

\bgcolor{black}{%
  \begin{frame}[plain]%
    \begin{center}%
      \includegraphics[height=\textheight,width=\textwidth,keepaspectratio]{soldering}%
    \end{center}%
  \end{frame}%
}

\begin{frame}[plain]%
  \begin{center}%
    \includegraphics[height=0.98\textheight,width=\textwidth,keepaspectratio]{laptop-exploded}%
  \end{center}%
\end{frame}%

\begin{frame}{Containers}
  \begin{itemize}
  \item Manage via machined and dynamic/transient units
  \item Provide virtual networks and DHCP server/client via networkd
  \item Handle hostname resolution via nss-resolved
  \item Configure kernel features for service lockdown
  \item Optional unified logging via journald
  \item Minimal filesystem: merged \texttt{/usr}, empty \texttt{/etc} and \texttt{/var}
  \item Configuration via firstboot and sysusers
  \item Automatically spin up container via socket activation
  \end{itemize}
\end{frame}

\begin{frame}{Eliminating maintainer scripts}
  \begin{itemize}
  \item Maintainer scripts make installs less reproducible
  \item We need more declarative configuration
  \item tmpfiles, sysusers, firstboot
  \end{itemize}
\end{frame}

\begin{frame}{Eliminating and consolidating wakeup sources}
  \begin{itemize}
  \item Eliminate periodic wakeups to check conditions
  \item Make conditions support select/poll/epoll
  \item Improve battery life
  \item Make performance more consistent
  \end{itemize}
\end{frame}

\begin{frame}{systemd user sessions}
  \begin{itemize}
  \item Manage user processes via systemd
  \item Replace gnome-session, Xsession, and similar
  \item Launch session components from a clean environment
  \item Launch components on-demand via activation
  \item Handle respawn, parallelism
  \item Handle new additions without session restart
  \item Handle common one-per-user services across multiple logins
  \item Support service lockdown and minimal privilege
  \end{itemize}
\end{frame}

\begin{frame}{Unifying user sessions}
  \begin{itemize}
  \item GNOME will replace gnome-session with systemd
  \item KDE plans the same
  \item Debian has \texttt{/etc/X11/Xsession.d}
  \item We should unify graphical user session startup:
    \begin{itemize}
    \item Non-environment-specific (\texttt{Xsession.d})
    \item Environment-specific services (gnome-session)
    \item Autostart services across desktops (XDG autostart)
    \item Sysadmin and user configuration (\texttt{/etc}, \texttt{\$HOME})
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Improving user choice and component selection}
  \begin{itemize}
  \item User can override individual .service or .socket units
    \begin{itemize}
    \item No administrator configuration required
    \end{itemize}
  \item Provide and depend on virtual units
    \begin{itemize}
    \item gpg-agent/ssh-agent versus gnome-keyring
    \end{itemize}
  \item \texttt{foo.target.wants} directories
  \item Easy to extend services with local configuration
    \begin{itemize}
    \item Run as user, lock down and limit privilege
    \end{itemize}
  \item Easy to mask services
  \item Run services as part of user sessions rather than systemwide
  \end{itemize}
\end{frame}

\begin{frame}
  \centering \Huge Discussion
\end{frame}
\end{document}
