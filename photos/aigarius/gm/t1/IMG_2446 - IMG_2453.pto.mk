
# makefile for panorama stitching, created by hugin using the new makefilelib

# Tool configuration
NONA=nona
PTSTITCHER=PTStitcher
PTMENDER=PTmender
PTBLENDER=PTblender
PTMASKER=PTmasker
PTROLLER=PTroller
ENBLEND=enblend
ENFUSE=enfuse
SMARTBLEND=smartblend.exe
HDRMERGE=hugin_hdrmerge
RM=rm
EXIFTOOL=exiftool

# Project parameters
HUGIN_PROJECTION=2
HUGIN_HFOV=84
HUGIN_WIDTH=11746
HUGIN_HEIGHT=14822

# options for the programs
NONA_LDR_REMAPPED_COMP=-z LZW 
NONA_OPTS=
ENBLEND_OPTS= -f7832x4291+2037+7316
ENBLEND_LDR_COMP=--compression=95
ENBLEND_EXPOSURE_COMP=--compression=LZW 
ENBLEND_HDR_COMP=
HDRMERGE_OPTS=-m avg -c
ENFUSE_OPTS=
EXIFTOOL_COPY_ARGS=-ImageDescription -Make -Model -Artist -WhitePoint -Copyright -GPS:all -DateTimeOriginal -CreateDate -UserComment -ColorSpace -OwnerName -SerialNumber
EXIFTOOL_INFO_ARGS='-Software=Hugin 2013.0.0.4692917e7a55' '-UserComment<$${UserComment}&\#xa;Projection: Equirectangular (2)&\#xa;FOV: 84 x 106&\#xa;Ev: 11.51' -f

# the output panorama
LDR_REMAPPED_PREFIX=IMG_2446\ -\ IMG_2453
LDR_REMAPPED_PREFIX_SHELL=IMG_2446\ -\ IMG_2453
HDR_STACK_REMAPPED_PREFIX=IMG_2446\ -\ IMG_2453_hdr_
HDR_STACK_REMAPPED_PREFIX_SHELL=IMG_2446\ -\ IMG_2453_hdr_
LDR_EXPOSURE_REMAPPED_PREFIX=IMG_2446\ -\ IMG_2453_exposure_layers_
LDR_EXPOSURE_REMAPPED_PREFIX_SHELL=IMG_2446\ -\ IMG_2453_exposure_layers_
PROJECT_FILE=/home/aigarius/Pictures/debconf14/gm/t1/IMG_2446\ -\ IMG_2453.pto
PROJECT_FILE_SHELL=/home/aigarius/Pictures/debconf14/gm/t1/IMG_2446\ -\ IMG_2453.pto
LDR_BLENDED=IMG_2446\ -\ IMG_2453.jpg
LDR_BLENDED_SHELL=IMG_2446\ -\ IMG_2453.jpg
LDR_STACKED_BLENDED=IMG_2446\ -\ IMG_2453_fused.jpg
LDR_STACKED_BLENDED_SHELL=IMG_2446\ -\ IMG_2453_fused.jpg
LDR_EXPOSURE_LAYERS_FUSED=IMG_2446\ -\ IMG_2453_blended_fused.jpg
LDR_EXPOSURE_LAYERS_FUSED_SHELL=IMG_2446\ -\ IMG_2453_blended_fused.jpg
HDR_BLENDED=IMG_2446\ -\ IMG_2453_hdr.exr
HDR_BLENDED_SHELL=IMG_2446\ -\ IMG_2453_hdr.exr

# first input image
INPUT_IMAGE_1=/home/aigarius/Pictures/debconf14/gm/t1/IMG_2446.JPG
INPUT_IMAGE_1_SHELL=/home/aigarius/Pictures/debconf14/gm/t1/IMG_2446.JPG

# all input images
INPUT_IMAGES=/home/aigarius/Pictures/debconf14/gm/t1/IMG_2446.JPG\
/home/aigarius/Pictures/debconf14/gm/t1/IMG_2449.JPG\
/home/aigarius/Pictures/debconf14/gm/t1/IMG_2451.JPG\
/home/aigarius/Pictures/debconf14/gm/t1/IMG_2453.JPG
INPUT_IMAGES_SHELL=/home/aigarius/Pictures/debconf14/gm/t1/IMG_2446.JPG\
/home/aigarius/Pictures/debconf14/gm/t1/IMG_2449.JPG\
/home/aigarius/Pictures/debconf14/gm/t1/IMG_2451.JPG\
/home/aigarius/Pictures/debconf14/gm/t1/IMG_2453.JPG

# remapped images
LDR_LAYERS=IMG_2446\ -\ IMG_24530000.tif\
IMG_2446\ -\ IMG_24530001.tif\
IMG_2446\ -\ IMG_24530002.tif\
IMG_2446\ -\ IMG_24530003.tif
LDR_LAYERS_SHELL=IMG_2446\ -\ IMG_24530000.tif\
IMG_2446\ -\ IMG_24530001.tif\
IMG_2446\ -\ IMG_24530002.tif\
IMG_2446\ -\ IMG_24530003.tif

# remapped images (hdr)
HDR_LAYERS=IMG_2446\ -\ IMG_2453_hdr_0000.exr\
IMG_2446\ -\ IMG_2453_hdr_0001.exr\
IMG_2446\ -\ IMG_2453_hdr_0002.exr\
IMG_2446\ -\ IMG_2453_hdr_0003.exr
HDR_LAYERS_SHELL=IMG_2446\ -\ IMG_2453_hdr_0000.exr\
IMG_2446\ -\ IMG_2453_hdr_0001.exr\
IMG_2446\ -\ IMG_2453_hdr_0002.exr\
IMG_2446\ -\ IMG_2453_hdr_0003.exr

# remapped maxval images
HDR_LAYERS_WEIGHTS=IMG_2446\ -\ IMG_2453_hdr_0000_gray.pgm\
IMG_2446\ -\ IMG_2453_hdr_0001_gray.pgm\
IMG_2446\ -\ IMG_2453_hdr_0002_gray.pgm\
IMG_2446\ -\ IMG_2453_hdr_0003_gray.pgm
HDR_LAYERS_WEIGHTS_SHELL=IMG_2446\ -\ IMG_2453_hdr_0000_gray.pgm\
IMG_2446\ -\ IMG_2453_hdr_0001_gray.pgm\
IMG_2446\ -\ IMG_2453_hdr_0002_gray.pgm\
IMG_2446\ -\ IMG_2453_hdr_0003_gray.pgm

# stacked hdr images
HDR_STACK_0=IMG_2446\ -\ IMG_2453_stack_hdr_0000.exr
HDR_STACK_0_SHELL=IMG_2446\ -\ IMG_2453_stack_hdr_0000.exr
HDR_STACK_0_INPUT=IMG_2446\ -\ IMG_2453_hdr_0000.exr
HDR_STACK_0_INPUT_SHELL=IMG_2446\ -\ IMG_2453_hdr_0000.exr
HDR_STACK_1=IMG_2446\ -\ IMG_2453_stack_hdr_0001.exr
HDR_STACK_1_SHELL=IMG_2446\ -\ IMG_2453_stack_hdr_0001.exr
HDR_STACK_1_INPUT=IMG_2446\ -\ IMG_2453_hdr_0001.exr
HDR_STACK_1_INPUT_SHELL=IMG_2446\ -\ IMG_2453_hdr_0001.exr
HDR_STACK_2=IMG_2446\ -\ IMG_2453_stack_hdr_0002.exr
HDR_STACK_2_SHELL=IMG_2446\ -\ IMG_2453_stack_hdr_0002.exr
HDR_STACK_2_INPUT=IMG_2446\ -\ IMG_2453_hdr_0002.exr
HDR_STACK_2_INPUT_SHELL=IMG_2446\ -\ IMG_2453_hdr_0002.exr
HDR_STACK_3=IMG_2446\ -\ IMG_2453_stack_hdr_0003.exr
HDR_STACK_3_SHELL=IMG_2446\ -\ IMG_2453_stack_hdr_0003.exr
HDR_STACK_3_INPUT=IMG_2446\ -\ IMG_2453_hdr_0003.exr
HDR_STACK_3_INPUT_SHELL=IMG_2446\ -\ IMG_2453_hdr_0003.exr
HDR_STACKS_NUMBERS=0 1 2 3 
HDR_STACKS=$(HDR_STACK_0) $(HDR_STACK_1) $(HDR_STACK_2) $(HDR_STACK_3) 
HDR_STACKS_SHELL=$(HDR_STACK_0_SHELL) $(HDR_STACK_1_SHELL) $(HDR_STACK_2_SHELL) $(HDR_STACK_3_SHELL) 

# number of image sets with similar exposure
LDR_EXPOSURE_LAYER_0=IMG_2446\ -\ IMG_2453_exposure_0000.tif
LDR_EXPOSURE_LAYER_0_SHELL=IMG_2446\ -\ IMG_2453_exposure_0000.tif
LDR_EXPOSURE_LAYER_0_INPUT=IMG_2446\ -\ IMG_2453_exposure_layers_0000.tif\
IMG_2446\ -\ IMG_2453_exposure_layers_0001.tif\
IMG_2446\ -\ IMG_2453_exposure_layers_0002.tif\
IMG_2446\ -\ IMG_2453_exposure_layers_0003.tif
LDR_EXPOSURE_LAYER_0_INPUT_SHELL=IMG_2446\ -\ IMG_2453_exposure_layers_0000.tif\
IMG_2446\ -\ IMG_2453_exposure_layers_0001.tif\
IMG_2446\ -\ IMG_2453_exposure_layers_0002.tif\
IMG_2446\ -\ IMG_2453_exposure_layers_0003.tif
LDR_EXPOSURE_LAYER_0_INPUT_PTMENDER=IMG_2446\ -\ IMG_24530000.tif\
IMG_2446\ -\ IMG_24530001.tif\
IMG_2446\ -\ IMG_24530002.tif\
IMG_2446\ -\ IMG_24530003.tif
LDR_EXPOSURE_LAYER_0_INPUT_PTMENDER_SHELL=IMG_2446\ -\ IMG_24530000.tif\
IMG_2446\ -\ IMG_24530001.tif\
IMG_2446\ -\ IMG_24530002.tif\
IMG_2446\ -\ IMG_24530003.tif
LDR_EXPOSURE_LAYER_0_EXPOSURE=11.5092
LDR_EXPOSURE_LAYERS_NUMBERS=0 
LDR_EXPOSURE_LAYERS=$(LDR_EXPOSURE_LAYER_0) 
LDR_EXPOSURE_LAYERS_SHELL=$(LDR_EXPOSURE_LAYER_0_SHELL) 
LDR_EXPOSURE_LAYERS_REMAPPED=IMG_2446\ -\ IMG_2453_exposure_layers_0000.tif\
IMG_2446\ -\ IMG_2453_exposure_layers_0001.tif\
IMG_2446\ -\ IMG_2453_exposure_layers_0002.tif\
IMG_2446\ -\ IMG_2453_exposure_layers_0003.tif
LDR_EXPOSURE_LAYERS_REMAPPED_SHELL=IMG_2446\ -\ IMG_2453_exposure_layers_0000.tif\
IMG_2446\ -\ IMG_2453_exposure_layers_0001.tif\
IMG_2446\ -\ IMG_2453_exposure_layers_0002.tif\
IMG_2446\ -\ IMG_2453_exposure_layers_0003.tif

# stacked ldr images
LDR_STACK_0=IMG_2446\ -\ IMG_2453_stack_ldr_0000.tif
LDR_STACK_0_SHELL=IMG_2446\ -\ IMG_2453_stack_ldr_0000.tif
LDR_STACK_0_INPUT=IMG_2446\ -\ IMG_2453_exposure_layers_0000.tif
LDR_STACK_0_INPUT_SHELL=IMG_2446\ -\ IMG_2453_exposure_layers_0000.tif
LDR_STACK_1=IMG_2446\ -\ IMG_2453_stack_ldr_0001.tif
LDR_STACK_1_SHELL=IMG_2446\ -\ IMG_2453_stack_ldr_0001.tif
LDR_STACK_1_INPUT=IMG_2446\ -\ IMG_2453_exposure_layers_0001.tif
LDR_STACK_1_INPUT_SHELL=IMG_2446\ -\ IMG_2453_exposure_layers_0001.tif
LDR_STACK_2=IMG_2446\ -\ IMG_2453_stack_ldr_0002.tif
LDR_STACK_2_SHELL=IMG_2446\ -\ IMG_2453_stack_ldr_0002.tif
LDR_STACK_2_INPUT=IMG_2446\ -\ IMG_2453_exposure_layers_0002.tif
LDR_STACK_2_INPUT_SHELL=IMG_2446\ -\ IMG_2453_exposure_layers_0002.tif
LDR_STACK_3=IMG_2446\ -\ IMG_2453_stack_ldr_0003.tif
LDR_STACK_3_SHELL=IMG_2446\ -\ IMG_2453_stack_ldr_0003.tif
LDR_STACK_3_INPUT=IMG_2446\ -\ IMG_2453_exposure_layers_0003.tif
LDR_STACK_3_INPUT_SHELL=IMG_2446\ -\ IMG_2453_exposure_layers_0003.tif
LDR_STACKS_NUMBERS=0 1 2 3 
LDR_STACKS=$(LDR_STACK_0) $(LDR_STACK_1) $(LDR_STACK_2) $(LDR_STACK_3) 
LDR_STACKS_SHELL=$(LDR_STACK_0_SHELL) $(LDR_STACK_1_SHELL) $(LDR_STACK_2_SHELL) $(LDR_STACK_3_SHELL) 
DO_LDR_BLENDED=1

all : startStitching $(LDR_BLENDED) 

startStitching : 
	@echo '==========================================================================='
	@echo 'Stitching panorama'
	@echo '==========================================================================='

clean : 
	@echo '==========================================================================='
	@echo 'Remove temporary files'
	@echo '==========================================================================='
	-$(RM) $(LDR_LAYERS_SHELL) 

test : 
	@echo '==========================================================================='
	@echo 'Testing programs'
	@echo '==========================================================================='
	@echo -n 'Checking nona...'
	@-$(NONA) --help > /dev/null 2>&1 && echo '[OK]' || echo '[FAILED]'
	@echo -n 'Checking enblend...'
	@-$(ENBLEND) -h > /dev/null 2>&1 && echo '[OK]' || echo '[FAILED]'
	@echo -n 'Checking enfuse...'
	@-$(ENFUSE) -h > /dev/null 2>&1 && echo '[OK]' || echo '[FAILED]'
	@echo -n 'Checking hugin_hdrmerge...'
	@-$(HDRMERGE) -h > /dev/null 2>&1 && echo '[OK]' || echo '[FAILED]'
	@echo -n 'Checking exiftool...'
	@-$(EXIFTOOL) -ver > /dev/null 2>&1 && echo '[OK]' || echo '[FAILED]'

info : 
	@echo '==========================================================================='
	@echo '***************  Panorama makefile generated by Hugin       ***************'
	@echo '==========================================================================='
	@echo 'System information'
	@echo '==========================================================================='
	@echo -n 'Operating system: '
	@-uname -o
	@echo -n 'Release: '
	@-uname -r
	@echo -n 'Kernel version: '
	@-uname -v
	@echo -n 'Machine: '
	@-uname -m
	@echo 'Disc usage'
	@-df -h
	@echo 'Memory usage'
	@-free -m
	@echo '==========================================================================='
	@echo 'Output options'
	@echo '==========================================================================='
	@echo 'Hugin Version: 2013.0.0.4692917e7a55'
	@echo 'Project file: /home/aigarius/Pictures/debconf14/gm/t1/IMG_2446 - IMG_2453.pto'
	@echo 'Output prefix: IMG_2446 - IMG_2453'
	@echo 'Projection: Equirectangular (2)'
	@echo 'Field of view: 84 x 106'
	@echo 'Canvas dimensions: 11746 x 14822'
	@echo 'Crop area: (2037,7316) - (9869,11607)'
	@echo 'Output exposure value: 11.51'
	@echo 'Output stacks minimum overlap: 0.700'
	@echo 'Output layers maximum Ev difference: 0.50'
	@echo 'Selected outputs'
	@echo 'Normal panorama'
	@echo '* Blended panorama'
	@echo '==========================================================================='
	@echo 'Input images'
	@echo '==========================================================================='
	@echo 'Number of images in project file: 4'
	@echo 'Number of active images: 4'
	@echo 'Image 0: /home/aigarius/Pictures/debconf14/gm/t1/IMG_2446.JPG'
	@echo 'Image 0: Size 5184x3456, Exposure: 11.65'
	@echo 'Image 1: /home/aigarius/Pictures/debconf14/gm/t1/IMG_2449.JPG'
	@echo 'Image 1: Size 5184x3456, Exposure: 11.50'
	@echo 'Image 2: /home/aigarius/Pictures/debconf14/gm/t1/IMG_2451.JPG'
	@echo 'Image 2: Size 5184x3456, Exposure: 11.63'
	@echo 'Image 3: /home/aigarius/Pictures/debconf14/gm/t1/IMG_2453.JPG'
	@echo 'Image 3: Size 5184x3456, Exposure: 11.26'

# Rules for ordinary TIFF_m and hdr output

IMG_2446\ -\ IMG_24530000.tif : /home/aigarius/Pictures/debconf14/gm/t1/IMG_2446.JPG $(PROJECT_FILE) 
	$(NONA) $(NONA_OPTS) $(NONA_LDR_REMAPPED_COMP) -r ldr -m TIFF_m -o $(LDR_REMAPPED_PREFIX_SHELL) -i 0 $(PROJECT_FILE_SHELL)

IMG_2446\ -\ IMG_2453_hdr_0000.exr : /home/aigarius/Pictures/debconf14/gm/t1/IMG_2446.JPG $(PROJECT_FILE) 
	$(NONA) $(NONA_OPTS) -r hdr -m EXR_m -o $(HDR_STACK_REMAPPED_PREFIX_SHELL) -i 0 $(PROJECT_FILE_SHELL)

IMG_2446\ -\ IMG_24530001.tif : /home/aigarius/Pictures/debconf14/gm/t1/IMG_2449.JPG $(PROJECT_FILE) 
	$(NONA) $(NONA_OPTS) $(NONA_LDR_REMAPPED_COMP) -r ldr -m TIFF_m -o $(LDR_REMAPPED_PREFIX_SHELL) -i 1 $(PROJECT_FILE_SHELL)

IMG_2446\ -\ IMG_2453_hdr_0001.exr : /home/aigarius/Pictures/debconf14/gm/t1/IMG_2449.JPG $(PROJECT_FILE) 
	$(NONA) $(NONA_OPTS) -r hdr -m EXR_m -o $(HDR_STACK_REMAPPED_PREFIX_SHELL) -i 1 $(PROJECT_FILE_SHELL)

IMG_2446\ -\ IMG_24530002.tif : /home/aigarius/Pictures/debconf14/gm/t1/IMG_2451.JPG $(PROJECT_FILE) 
	$(NONA) $(NONA_OPTS) $(NONA_LDR_REMAPPED_COMP) -r ldr -m TIFF_m -o $(LDR_REMAPPED_PREFIX_SHELL) -i 2 $(PROJECT_FILE_SHELL)

IMG_2446\ -\ IMG_2453_hdr_0002.exr : /home/aigarius/Pictures/debconf14/gm/t1/IMG_2451.JPG $(PROJECT_FILE) 
	$(NONA) $(NONA_OPTS) -r hdr -m EXR_m -o $(HDR_STACK_REMAPPED_PREFIX_SHELL) -i 2 $(PROJECT_FILE_SHELL)

IMG_2446\ -\ IMG_24530003.tif : /home/aigarius/Pictures/debconf14/gm/t1/IMG_2453.JPG $(PROJECT_FILE) 
	$(NONA) $(NONA_OPTS) $(NONA_LDR_REMAPPED_COMP) -r ldr -m TIFF_m -o $(LDR_REMAPPED_PREFIX_SHELL) -i 3 $(PROJECT_FILE_SHELL)

IMG_2446\ -\ IMG_2453_hdr_0003.exr : /home/aigarius/Pictures/debconf14/gm/t1/IMG_2453.JPG $(PROJECT_FILE) 
	$(NONA) $(NONA_OPTS) -r hdr -m EXR_m -o $(HDR_STACK_REMAPPED_PREFIX_SHELL) -i 3 $(PROJECT_FILE_SHELL)

# Rules for exposure layer output

IMG_2446\ -\ IMG_2453_exposure_layers_0000.tif : /home/aigarius/Pictures/debconf14/gm/t1/IMG_2446.JPG $(PROJECT_FILE) 
	$(NONA) $(NONA_OPTS) $(NONA_LDR_REMAPPED_COMP) -r ldr -e 11.6463 -m TIFF_m -o $(LDR_EXPOSURE_REMAPPED_PREFIX_SHELL) -i 0 $(PROJECT_FILE_SHELL)

IMG_2446\ -\ IMG_2453_exposure_layers_0001.tif : /home/aigarius/Pictures/debconf14/gm/t1/IMG_2449.JPG $(PROJECT_FILE) 
	$(NONA) $(NONA_OPTS) $(NONA_LDR_REMAPPED_COMP) -r ldr -e 11.4994 -m TIFF_m -o $(LDR_EXPOSURE_REMAPPED_PREFIX_SHELL) -i 1 $(PROJECT_FILE_SHELL)

IMG_2446\ -\ IMG_2453_exposure_layers_0002.tif : /home/aigarius/Pictures/debconf14/gm/t1/IMG_2451.JPG $(PROJECT_FILE) 
	$(NONA) $(NONA_OPTS) $(NONA_LDR_REMAPPED_COMP) -r ldr -e 11.6326 -m TIFF_m -o $(LDR_EXPOSURE_REMAPPED_PREFIX_SHELL) -i 2 $(PROJECT_FILE_SHELL)

IMG_2446\ -\ IMG_2453_exposure_layers_0003.tif : /home/aigarius/Pictures/debconf14/gm/t1/IMG_2453.JPG $(PROJECT_FILE) 
	$(NONA) $(NONA_OPTS) $(NONA_LDR_REMAPPED_COMP) -r ldr -e 11.2585 -m TIFF_m -o $(LDR_EXPOSURE_REMAPPED_PREFIX_SHELL) -i 3 $(PROJECT_FILE_SHELL)

# Rules for LDR and HDR stack merging, a rule for each stack

$(LDR_STACK_0) : $(LDR_STACK_0_INPUT) 
	$(ENFUSE) $(ENFUSE_OPTS) -o $(LDR_STACK_0_SHELL) -- $(LDR_STACK_0_INPUT_SHELL)
	-$(EXIFTOOL) -overwrite_original_in_place -TagsFromFile $(INPUT_IMAGE_1_SHELL) $(EXIFTOOL_COPY_ARGS) $(LDR_STACK_0_SHELL)

$(HDR_STACK_0) : $(HDR_STACK_0_INPUT) 
	$(HDRMERGE) $(HDRMERGE_OPTS) -o $(HDR_STACK_0_SHELL) -- $(HDR_STACK_0_INPUT_SHELL)

$(LDR_STACK_1) : $(LDR_STACK_1_INPUT) 
	$(ENFUSE) $(ENFUSE_OPTS) -o $(LDR_STACK_1_SHELL) -- $(LDR_STACK_1_INPUT_SHELL)
	-$(EXIFTOOL) -overwrite_original_in_place -TagsFromFile $(INPUT_IMAGE_1_SHELL) $(EXIFTOOL_COPY_ARGS) $(LDR_STACK_1_SHELL)

$(HDR_STACK_1) : $(HDR_STACK_1_INPUT) 
	$(HDRMERGE) $(HDRMERGE_OPTS) -o $(HDR_STACK_1_SHELL) -- $(HDR_STACK_1_INPUT_SHELL)

$(LDR_STACK_2) : $(LDR_STACK_2_INPUT) 
	$(ENFUSE) $(ENFUSE_OPTS) -o $(LDR_STACK_2_SHELL) -- $(LDR_STACK_2_INPUT_SHELL)
	-$(EXIFTOOL) -overwrite_original_in_place -TagsFromFile $(INPUT_IMAGE_1_SHELL) $(EXIFTOOL_COPY_ARGS) $(LDR_STACK_2_SHELL)

$(HDR_STACK_2) : $(HDR_STACK_2_INPUT) 
	$(HDRMERGE) $(HDRMERGE_OPTS) -o $(HDR_STACK_2_SHELL) -- $(HDR_STACK_2_INPUT_SHELL)

$(LDR_STACK_3) : $(LDR_STACK_3_INPUT) 
	$(ENFUSE) $(ENFUSE_OPTS) -o $(LDR_STACK_3_SHELL) -- $(LDR_STACK_3_INPUT_SHELL)
	-$(EXIFTOOL) -overwrite_original_in_place -TagsFromFile $(INPUT_IMAGE_1_SHELL) $(EXIFTOOL_COPY_ARGS) $(LDR_STACK_3_SHELL)

$(HDR_STACK_3) : $(HDR_STACK_3_INPUT) 
	$(HDRMERGE) $(HDRMERGE_OPTS) -o $(HDR_STACK_3_SHELL) -- $(HDR_STACK_3_INPUT_SHELL)

$(LDR_BLENDED) : $(LDR_LAYERS) 
	$(ENBLEND) $(ENBLEND_LDR_COMP) $(ENBLEND_OPTS) -o $(LDR_BLENDED_SHELL) -- $(LDR_LAYERS_SHELL)
	-$(EXIFTOOL) -E -overwrite_original_in_place -TagsFromFile $(INPUT_IMAGE_1_SHELL) $(EXIFTOOL_COPY_ARGS) $(EXIFTOOL_INFO_ARGS) $(LDR_BLENDED_SHELL)

$(LDR_EXPOSURE_LAYER_0) : $(LDR_EXPOSURE_LAYER_0_INPUT) 
	$(ENBLEND) $(ENBLEND_EXPOSURE_COMP) $(ENBLEND_OPTS) -o $(LDR_EXPOSURE_LAYER_0_SHELL) -- $(LDR_EXPOSURE_LAYER_0_INPUT_SHELL)
	-$(EXIFTOOL) -overwrite_original_in_place -TagsFromFile $(INPUT_IMAGE_1_SHELL) $(EXIFTOOL_COPY_ARGS) $(LDR_EXPOSURE_LAYER_0_SHELL)

$(LDR_STACKED_BLENDED) : $(LDR_STACKS) 
	$(ENBLEND) $(ENBLEND_LDR_COMP) $(ENBLEND_OPTS) -o $(LDR_STACKED_BLENDED_SHELL) -- $(LDR_STACKS_SHELL)
	-$(EXIFTOOL) -E -overwrite_original_in_place -TagsFromFile $(INPUT_IMAGE_1_SHELL) $(EXIFTOOL_COPY_ARGS) $(EXIFTOOL_INFO_ARGS) $(LDR_STACKED_BLENDED_SHELL)

$(LDR_EXPOSURE_LAYERS_FUSED) : $(LDR_EXPOSURE_LAYERS) 
	$(ENFUSE) $(ENBLEND_LDR_COMP) $(ENFUSE_OPTS) -o $(LDR_EXPOSURE_LAYERS_FUSED_SHELL) -- $(LDR_EXPOSURE_LAYERS_SHELL)
	-$(EXIFTOOL) -E -overwrite_original_in_place -TagsFromFile $(INPUT_IMAGE_1_SHELL) $(EXIFTOOL_COPY_ARGS) $(EXIFTOOL_INFO_ARGS) $(LDR_EXPOSURE_LAYERS_FUSED_SHELL)

$(HDR_BLENDED) : $(HDR_STACKS) 
	$(ENBLEND) $(ENBLEND_HDR_COMP) $(ENBLEND_OPTS) -o $(HDR_BLENDED_SHELL) -- $(HDR_STACKS_SHELL)

$(LDR_REMAPPED_PREFIX)_multilayer.tif : $(LDR_LAYERS) 
	tiffcp $(LDR_LAYERS_SHELL) $(LDR_REMAPPED_PREFIX_SHELL)_multilayer.tif

$(LDR_REMAPPED_PREFIX)_fused_multilayer.tif : $(LDR_STACKS) $(LDR_EXPOSURE_LAYERS) 
	tiffcp $(LDR_STACKS_SHELL) $(LDR_EXPOSURE_LAYERS_SHELL) $(LDR_REMAPPED_PREFIX_SHELL)_fused_multilayer.tif

$(LDR_REMAPPED_PREFIX)_multilayer.psd : $(LDR_LAYERS) 
	PTtiff2psd -o $(LDR_REMAPPED_PREFIX_SHELL)_multilayer.psd $(LDR_LAYERS_SHELL)

$(LDR_REMAPPED_PREFIX)_fused_multilayer.psd : $(LDR_STACKS) $(LDR_EXPOSURE_LAYERS) 
	PTtiff2psd -o $(LDR_REMAPPED_PREFIX_SHELL)_fused_multilayer.psd $(LDR_STACKS_SHELL)$(LDR_EXPOSURE_LAYERS_SHELL)
