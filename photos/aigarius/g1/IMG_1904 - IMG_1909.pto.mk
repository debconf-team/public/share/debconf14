
# makefile for panorama stitching, created by hugin using the new makefilelib

# Tool configuration
NONA=nona
PTSTITCHER=PTStitcher
PTMENDER=PTmender
PTBLENDER=PTblender
PTMASKER=PTmasker
PTROLLER=PTroller
ENBLEND=enblend
ENFUSE=enfuse
SMARTBLEND=smartblend.exe
HDRMERGE=hugin_hdrmerge
RM=rm
EXIFTOOL=exiftool

# Project parameters
HUGIN_PROJECTION=0
HUGIN_HFOV=40
HUGIN_WIDTH=4082
HUGIN_HEIGHT=5919

# options for the programs
NONA_LDR_REMAPPED_COMP=-z LZW 
NONA_OPTS=
ENBLEND_OPTS= -f3451x3407+354+2512
ENBLEND_LDR_COMP=--compression=95
ENBLEND_EXPOSURE_COMP=--compression=LZW 
ENBLEND_HDR_COMP=
HDRMERGE_OPTS=-m avg -c
ENFUSE_OPTS=
EXIFTOOL_COPY_ARGS=-ImageDescription -Make -Model -Artist -WhitePoint -Copyright -GPS:all -DateTimeOriginal -CreateDate -UserComment -ColorSpace -OwnerName -SerialNumber
EXIFTOOL_INFO_ARGS='-Software=Hugin 2013.0.0.4692917e7a55' '-UserComment<$${UserComment}&\#xa;Projection: Rectilinear (0)&\#xa;FOV: 40 x 56&\#xa;Ev: 6.39' -f

# the output panorama
LDR_REMAPPED_PREFIX=IMG_1904\ -\ IMG_1909
LDR_REMAPPED_PREFIX_SHELL=IMG_1904\ -\ IMG_1909
HDR_STACK_REMAPPED_PREFIX=IMG_1904\ -\ IMG_1909_hdr_
HDR_STACK_REMAPPED_PREFIX_SHELL=IMG_1904\ -\ IMG_1909_hdr_
LDR_EXPOSURE_REMAPPED_PREFIX=IMG_1904\ -\ IMG_1909_exposure_layers_
LDR_EXPOSURE_REMAPPED_PREFIX_SHELL=IMG_1904\ -\ IMG_1909_exposure_layers_
PROJECT_FILE=/home/aigarius/Pictures/debconf14/IMG_1904\ -\ IMG_1909.pto
PROJECT_FILE_SHELL=/home/aigarius/Pictures/debconf14/IMG_1904\ -\ IMG_1909.pto
LDR_BLENDED=IMG_1904\ -\ IMG_1909.jpg
LDR_BLENDED_SHELL=IMG_1904\ -\ IMG_1909.jpg
LDR_STACKED_BLENDED=IMG_1904\ -\ IMG_1909_fused.jpg
LDR_STACKED_BLENDED_SHELL=IMG_1904\ -\ IMG_1909_fused.jpg
LDR_EXPOSURE_LAYERS_FUSED=IMG_1904\ -\ IMG_1909_blended_fused.jpg
LDR_EXPOSURE_LAYERS_FUSED_SHELL=IMG_1904\ -\ IMG_1909_blended_fused.jpg
HDR_BLENDED=IMG_1904\ -\ IMG_1909_hdr.exr
HDR_BLENDED_SHELL=IMG_1904\ -\ IMG_1909_hdr.exr

# first input image
INPUT_IMAGE_1=/home/aigarius/Pictures/debconf14/IMG_1904.JPG
INPUT_IMAGE_1_SHELL=/home/aigarius/Pictures/debconf14/IMG_1904.JPG

# all input images
INPUT_IMAGES=/home/aigarius/Pictures/debconf14/IMG_1904.JPG\
/home/aigarius/Pictures/debconf14/IMG_1905.JPG\
/home/aigarius/Pictures/debconf14/IMG_1906.JPG\
/home/aigarius/Pictures/debconf14/IMG_1907.JPG\
/home/aigarius/Pictures/debconf14/IMG_1908.JPG\
/home/aigarius/Pictures/debconf14/IMG_1909.JPG
INPUT_IMAGES_SHELL=/home/aigarius/Pictures/debconf14/IMG_1904.JPG\
/home/aigarius/Pictures/debconf14/IMG_1905.JPG\
/home/aigarius/Pictures/debconf14/IMG_1906.JPG\
/home/aigarius/Pictures/debconf14/IMG_1907.JPG\
/home/aigarius/Pictures/debconf14/IMG_1908.JPG\
/home/aigarius/Pictures/debconf14/IMG_1909.JPG

# remapped images
LDR_LAYERS=IMG_1904\ -\ IMG_19090003.tif\
IMG_1904\ -\ IMG_19090005.tif
LDR_LAYERS_SHELL=IMG_1904\ -\ IMG_19090003.tif\
IMG_1904\ -\ IMG_19090005.tif

# remapped images (hdr)
HDR_LAYERS=IMG_1904\ -\ IMG_1909_hdr_0003.exr\
IMG_1904\ -\ IMG_1909_hdr_0005.exr
HDR_LAYERS_SHELL=IMG_1904\ -\ IMG_1909_hdr_0003.exr\
IMG_1904\ -\ IMG_1909_hdr_0005.exr

# remapped maxval images
HDR_LAYERS_WEIGHTS=IMG_1904\ -\ IMG_1909_hdr_0003_gray.pgm\
IMG_1904\ -\ IMG_1909_hdr_0005_gray.pgm
HDR_LAYERS_WEIGHTS_SHELL=IMG_1904\ -\ IMG_1909_hdr_0003_gray.pgm\
IMG_1904\ -\ IMG_1909_hdr_0005_gray.pgm

# stacked hdr images
HDR_STACK_0=IMG_1904\ -\ IMG_1909_stack_hdr_0000.exr
HDR_STACK_0_SHELL=IMG_1904\ -\ IMG_1909_stack_hdr_0000.exr
HDR_STACK_0_INPUT=IMG_1904\ -\ IMG_1909_hdr_0003.exr
HDR_STACK_0_INPUT_SHELL=IMG_1904\ -\ IMG_1909_hdr_0003.exr
HDR_STACK_1=IMG_1904\ -\ IMG_1909_stack_hdr_0001.exr
HDR_STACK_1_SHELL=IMG_1904\ -\ IMG_1909_stack_hdr_0001.exr
HDR_STACK_1_INPUT=IMG_1904\ -\ IMG_1909_hdr_0005.exr
HDR_STACK_1_INPUT_SHELL=IMG_1904\ -\ IMG_1909_hdr_0005.exr
HDR_STACKS_NUMBERS=0 1 
HDR_STACKS=$(HDR_STACK_0) $(HDR_STACK_1) 
HDR_STACKS_SHELL=$(HDR_STACK_0_SHELL) $(HDR_STACK_1_SHELL) 

# number of image sets with similar exposure
LDR_EXPOSURE_LAYER_0=IMG_1904\ -\ IMG_1909_exposure_0000.tif
LDR_EXPOSURE_LAYER_0_SHELL=IMG_1904\ -\ IMG_1909_exposure_0000.tif
LDR_EXPOSURE_LAYER_0_INPUT=IMG_1904\ -\ IMG_1909_exposure_layers_0003.tif\
IMG_1904\ -\ IMG_1909_exposure_layers_0005.tif
LDR_EXPOSURE_LAYER_0_INPUT_SHELL=IMG_1904\ -\ IMG_1909_exposure_layers_0003.tif\
IMG_1904\ -\ IMG_1909_exposure_layers_0005.tif
LDR_EXPOSURE_LAYER_0_INPUT_PTMENDER=IMG_1904\ -\ IMG_19090003.tif\
IMG_1904\ -\ IMG_19090005.tif
LDR_EXPOSURE_LAYER_0_INPUT_PTMENDER_SHELL=IMG_1904\ -\ IMG_19090003.tif\
IMG_1904\ -\ IMG_19090005.tif
LDR_EXPOSURE_LAYER_0_EXPOSURE=6.26211
LDR_EXPOSURE_LAYERS_NUMBERS=0 
LDR_EXPOSURE_LAYERS=$(LDR_EXPOSURE_LAYER_0) 
LDR_EXPOSURE_LAYERS_SHELL=$(LDR_EXPOSURE_LAYER_0_SHELL) 
LDR_EXPOSURE_LAYERS_REMAPPED=IMG_1904\ -\ IMG_1909_exposure_layers_0003.tif\
IMG_1904\ -\ IMG_1909_exposure_layers_0005.tif
LDR_EXPOSURE_LAYERS_REMAPPED_SHELL=IMG_1904\ -\ IMG_1909_exposure_layers_0003.tif\
IMG_1904\ -\ IMG_1909_exposure_layers_0005.tif

# stacked ldr images
LDR_STACK_0=IMG_1904\ -\ IMG_1909_stack_ldr_0000.tif
LDR_STACK_0_SHELL=IMG_1904\ -\ IMG_1909_stack_ldr_0000.tif
LDR_STACK_0_INPUT=IMG_1904\ -\ IMG_1909_exposure_layers_0003.tif
LDR_STACK_0_INPUT_SHELL=IMG_1904\ -\ IMG_1909_exposure_layers_0003.tif
LDR_STACK_1=IMG_1904\ -\ IMG_1909_stack_ldr_0001.tif
LDR_STACK_1_SHELL=IMG_1904\ -\ IMG_1909_stack_ldr_0001.tif
LDR_STACK_1_INPUT=IMG_1904\ -\ IMG_1909_exposure_layers_0005.tif
LDR_STACK_1_INPUT_SHELL=IMG_1904\ -\ IMG_1909_exposure_layers_0005.tif
LDR_STACKS_NUMBERS=0 1 
LDR_STACKS=$(LDR_STACK_0) $(LDR_STACK_1) 
LDR_STACKS_SHELL=$(LDR_STACK_0_SHELL) $(LDR_STACK_1_SHELL) 
DO_LDR_STACKED_BLENDED=1

all : startStitching $(LDR_STACKED_BLENDED) 

startStitching : 
	@echo '==========================================================================='
	@echo 'Stitching panorama'
	@echo '==========================================================================='

clean : 
	@echo '==========================================================================='
	@echo 'Remove temporary files'
	@echo '==========================================================================='
	-$(RM) $(LDR_STACKS_SHELL) $(LDR_EXPOSURE_LAYERS_REMAPPED_SHELL) 

test : 
	@echo '==========================================================================='
	@echo 'Testing programs'
	@echo '==========================================================================='
	@echo -n 'Checking nona...'
	@-$(NONA) --help > /dev/null 2>&1 && echo '[OK]' || echo '[FAILED]'
	@echo -n 'Checking enblend...'
	@-$(ENBLEND) -h > /dev/null 2>&1 && echo '[OK]' || echo '[FAILED]'
	@echo -n 'Checking enfuse...'
	@-$(ENFUSE) -h > /dev/null 2>&1 && echo '[OK]' || echo '[FAILED]'
	@echo -n 'Checking hugin_hdrmerge...'
	@-$(HDRMERGE) -h > /dev/null 2>&1 && echo '[OK]' || echo '[FAILED]'
	@echo -n 'Checking exiftool...'
	@-$(EXIFTOOL) -ver > /dev/null 2>&1 && echo '[OK]' || echo '[FAILED]'

info : 
	@echo '==========================================================================='
	@echo '***************  Panorama makefile generated by Hugin       ***************'
	@echo '==========================================================================='
	@echo 'System information'
	@echo '==========================================================================='
	@echo -n 'Operating system: '
	@-uname -o
	@echo -n 'Release: '
	@-uname -r
	@echo -n 'Kernel version: '
	@-uname -v
	@echo -n 'Machine: '
	@-uname -m
	@echo 'Disc usage'
	@-df -h
	@echo 'Memory usage'
	@-free -m
	@echo '==========================================================================='
	@echo 'Output options'
	@echo '==========================================================================='
	@echo 'Hugin Version: 2013.0.0.4692917e7a55'
	@echo 'Project file: /home/aigarius/Pictures/debconf14/IMG_1904 - IMG_1909.pto'
	@echo 'Output prefix: IMG_1904 - IMG_1909'
	@echo 'Projection: Rectilinear (0)'
	@echo 'Field of view: 40 x 56'
	@echo 'Canvas dimensions: 4082 x 5919'
	@echo 'Crop area: (354,2512) - (3805,5919)'
	@echo 'Output exposure value: 6.39'
	@echo 'Output stacks minimum overlap: 0.700'
	@echo 'Output layers maximum Ev difference: 0.50'
	@echo 'Selected outputs'
	@echo 'Exposure fusion'
	@echo '* Fused and blended panorama'
	@echo '==========================================================================='
	@echo 'Input images'
	@echo '==========================================================================='
	@echo 'Number of images in project file: 6'
	@echo 'Number of active images: 2'
	@echo 'Image 3: /home/aigarius/Pictures/debconf14/IMG_1907.JPG'
	@echo 'Image 3: Size 5184x3456, Exposure: 6.35'
	@echo 'Image 5: /home/aigarius/Pictures/debconf14/IMG_1909.JPG'
	@echo 'Image 5: Size 5184x3456, Exposure: 6.18'

# Rules for ordinary TIFF_m and hdr output

IMG_1904\ -\ IMG_19090003.tif : /home/aigarius/Pictures/debconf14/IMG_1907.JPG $(PROJECT_FILE) 
	$(NONA) $(NONA_OPTS) $(NONA_LDR_REMAPPED_COMP) -r ldr -m TIFF_m -o $(LDR_REMAPPED_PREFIX_SHELL) -i 3 $(PROJECT_FILE_SHELL)

IMG_1904\ -\ IMG_1909_hdr_0003.exr : /home/aigarius/Pictures/debconf14/IMG_1907.JPG $(PROJECT_FILE) 
	$(NONA) $(NONA_OPTS) -r hdr -m EXR_m -o $(HDR_STACK_REMAPPED_PREFIX_SHELL) -i 3 $(PROJECT_FILE_SHELL)

IMG_1904\ -\ IMG_19090005.tif : /home/aigarius/Pictures/debconf14/IMG_1909.JPG $(PROJECT_FILE) 
	$(NONA) $(NONA_OPTS) $(NONA_LDR_REMAPPED_COMP) -r ldr -m TIFF_m -o $(LDR_REMAPPED_PREFIX_SHELL) -i 5 $(PROJECT_FILE_SHELL)

IMG_1904\ -\ IMG_1909_hdr_0005.exr : /home/aigarius/Pictures/debconf14/IMG_1909.JPG $(PROJECT_FILE) 
	$(NONA) $(NONA_OPTS) -r hdr -m EXR_m -o $(HDR_STACK_REMAPPED_PREFIX_SHELL) -i 5 $(PROJECT_FILE_SHELL)

# Rules for exposure layer output

IMG_1904\ -\ IMG_1909_exposure_layers_0003.tif : /home/aigarius/Pictures/debconf14/IMG_1907.JPG $(PROJECT_FILE) 
	$(NONA) $(NONA_OPTS) $(NONA_LDR_REMAPPED_COMP) -r ldr -e 6.3479 -m TIFF_m -o $(LDR_EXPOSURE_REMAPPED_PREFIX_SHELL) -i 3 $(PROJECT_FILE_SHELL)

IMG_1904\ -\ IMG_1909_exposure_layers_0005.tif : /home/aigarius/Pictures/debconf14/IMG_1909.JPG $(PROJECT_FILE) 
	$(NONA) $(NONA_OPTS) $(NONA_LDR_REMAPPED_COMP) -r ldr -e 6.17632 -m TIFF_m -o $(LDR_EXPOSURE_REMAPPED_PREFIX_SHELL) -i 5 $(PROJECT_FILE_SHELL)

# Rules for LDR and HDR stack merging, a rule for each stack

$(LDR_STACK_0) : $(LDR_STACK_0_INPUT) 
	$(ENFUSE) $(ENFUSE_OPTS) -o $(LDR_STACK_0_SHELL) -- $(LDR_STACK_0_INPUT_SHELL)
	-$(EXIFTOOL) -overwrite_original_in_place -TagsFromFile $(INPUT_IMAGE_1_SHELL) $(EXIFTOOL_COPY_ARGS) $(LDR_STACK_0_SHELL)

$(HDR_STACK_0) : $(HDR_STACK_0_INPUT) 
	$(HDRMERGE) $(HDRMERGE_OPTS) -o $(HDR_STACK_0_SHELL) -- $(HDR_STACK_0_INPUT_SHELL)

$(LDR_STACK_1) : $(LDR_STACK_1_INPUT) 
	$(ENFUSE) $(ENFUSE_OPTS) -o $(LDR_STACK_1_SHELL) -- $(LDR_STACK_1_INPUT_SHELL)
	-$(EXIFTOOL) -overwrite_original_in_place -TagsFromFile $(INPUT_IMAGE_1_SHELL) $(EXIFTOOL_COPY_ARGS) $(LDR_STACK_1_SHELL)

$(HDR_STACK_1) : $(HDR_STACK_1_INPUT) 
	$(HDRMERGE) $(HDRMERGE_OPTS) -o $(HDR_STACK_1_SHELL) -- $(HDR_STACK_1_INPUT_SHELL)

$(LDR_BLENDED) : $(LDR_LAYERS) 
	$(ENBLEND) $(ENBLEND_LDR_COMP) $(ENBLEND_OPTS) -o $(LDR_BLENDED_SHELL) -- $(LDR_LAYERS_SHELL)
	-$(EXIFTOOL) -E -overwrite_original_in_place -TagsFromFile $(INPUT_IMAGE_1_SHELL) $(EXIFTOOL_COPY_ARGS) $(EXIFTOOL_INFO_ARGS) $(LDR_BLENDED_SHELL)

$(LDR_EXPOSURE_LAYER_0) : $(LDR_EXPOSURE_LAYER_0_INPUT) 
	$(ENBLEND) $(ENBLEND_EXPOSURE_COMP) $(ENBLEND_OPTS) -o $(LDR_EXPOSURE_LAYER_0_SHELL) -- $(LDR_EXPOSURE_LAYER_0_INPUT_SHELL)
	-$(EXIFTOOL) -overwrite_original_in_place -TagsFromFile $(INPUT_IMAGE_1_SHELL) $(EXIFTOOL_COPY_ARGS) $(LDR_EXPOSURE_LAYER_0_SHELL)

$(LDR_STACKED_BLENDED) : $(LDR_STACKS) 
	$(ENBLEND) $(ENBLEND_LDR_COMP) $(ENBLEND_OPTS) -o $(LDR_STACKED_BLENDED_SHELL) -- $(LDR_STACKS_SHELL)
	-$(EXIFTOOL) -E -overwrite_original_in_place -TagsFromFile $(INPUT_IMAGE_1_SHELL) $(EXIFTOOL_COPY_ARGS) $(EXIFTOOL_INFO_ARGS) $(LDR_STACKED_BLENDED_SHELL)

$(LDR_EXPOSURE_LAYERS_FUSED) : $(LDR_EXPOSURE_LAYERS) 
	$(ENFUSE) $(ENBLEND_LDR_COMP) $(ENFUSE_OPTS) -o $(LDR_EXPOSURE_LAYERS_FUSED_SHELL) -- $(LDR_EXPOSURE_LAYERS_SHELL)
	-$(EXIFTOOL) -E -overwrite_original_in_place -TagsFromFile $(INPUT_IMAGE_1_SHELL) $(EXIFTOOL_COPY_ARGS) $(EXIFTOOL_INFO_ARGS) $(LDR_EXPOSURE_LAYERS_FUSED_SHELL)

$(HDR_BLENDED) : $(HDR_STACKS) 
	$(ENBLEND) $(ENBLEND_HDR_COMP) $(ENBLEND_OPTS) -o $(HDR_BLENDED_SHELL) -- $(HDR_STACKS_SHELL)

$(LDR_REMAPPED_PREFIX)_multilayer.tif : $(LDR_LAYERS) 
	tiffcp $(LDR_LAYERS_SHELL) $(LDR_REMAPPED_PREFIX_SHELL)_multilayer.tif

$(LDR_REMAPPED_PREFIX)_fused_multilayer.tif : $(LDR_STACKS) $(LDR_EXPOSURE_LAYERS) 
	tiffcp $(LDR_STACKS_SHELL) $(LDR_EXPOSURE_LAYERS_SHELL) $(LDR_REMAPPED_PREFIX_SHELL)_fused_multilayer.tif

$(LDR_REMAPPED_PREFIX)_multilayer.psd : $(LDR_LAYERS) 
	PTtiff2psd -o $(LDR_REMAPPED_PREFIX_SHELL)_multilayer.psd $(LDR_LAYERS_SHELL)

$(LDR_REMAPPED_PREFIX)_fused_multilayer.psd : $(LDR_STACKS) $(LDR_EXPOSURE_LAYERS) 
	PTtiff2psd -o $(LDR_REMAPPED_PREFIX_SHELL)_fused_multilayer.psd $(LDR_STACKS_SHELL)$(LDR_EXPOSURE_LAYERS_SHELL)
